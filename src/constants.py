ID, IDX, REPLY_ID, WEIGHT = 'id', 'index', 'reply_to_message_id', 'weight'
ID_NA_FILLER = -1
DATE, TEXT = 'date', 'text'
TARGET = 'y'

# take all the columns that are left after reading to featurization
# PAIRS_COLS = [ID, DATE, 'from', 'from_id', 'edited', TEXT]
SUFFIXES = ('_orig', '_repl')