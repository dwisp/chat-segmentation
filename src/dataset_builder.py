import src.constants as c
import pandas as pd
import numpy as np
from sentence_transformers import SentenceTransformer


def sample_negative_pairs(
        chat: pd.DataFrame, n_neg_samples: int = 25, window_width: int = 100, random_state: int = 451
) -> pd.DataFrame:
    """
    Generates message pairs from the chat dataframe, positive + sampled negative
    Could be used to generate inference features by setting :n_neg_samples: = :window_width:
    This way the entire window of window_width (both pos and neg) after each message is generated
    """
    neg_samples, index = {}, chat.index
    map_idx2id, map_id2idx = dict(zip(index, chat[c.ID])), dict(zip(chat[c.ID], index))
    # message id -> set(responses to that message)
    map_reply_idx2idx = (
        chat
        .loc[:, [c.ID, c.REPLY_ID]]
        .dropna(subset=c.REPLY_ID)
        .assign(**{
            c.ID: lambda row: [map_id2idx[id] for id in row[c.ID]],
            # there are messages removed from the pool to which replies existed
            # mapping their ids to none and droping them in the next step
            c.REPLY_ID: lambda row: [map_id2idx.get(id) for id in row[c.REPLY_ID]],
        })
        .dropna()
        .groupby(c.REPLY_ID)
        .agg({c.ID: set})
        .to_dict(orient='index')
    )
    map_replyidx_2idx = {rep_id: dict_ids[c.ID] for rep_id, dict_ids in map_reply_idx2idx.items()}

    np.random.seed(random_state)
    for msg_idx in index:
        replies = map_replyidx_2idx.get(msg_idx, set())
        support = [idx for idx in range(msg_idx + 1, min(index.max(), msg_idx + window_width + 1)) if idx not in replies]
        neg_samples[msg_idx] = np.random.choice(support, min(n_neg_samples, len(support)), replace=False).tolist()
    neg_samples = (
        pd.DataFrame({c.ID: neg_samples.keys(), c.REPLY_ID: neg_samples.values()}, columns=[c.ID, c.REPLY_ID])
        .explode(c.REPLY_ID)
        .dropna()  # избавляемся от сообщений без пары
        .assign(**{
            c.ID: lambda row: [map_idx2id[id] for id in row[c.ID]],
            c.REPLY_ID: lambda row: [map_idx2id[id] for id in row[c.REPLY_ID]],
        })
    )
    # восстанавливаем оригинальные сообщения и джойним к ним негативные пары
    neg_samples = pd.merge(neg_samples, chat.drop(columns=c.REPLY_ID), on=c.ID)
    neg_samples = pd.merge(neg_samples, chat.drop(columns=c.REPLY_ID), left_on=c.REPLY_ID, right_on=c.ID, suffixes=c.SUFFIXES)
    return neg_samples


def add_embeddings(chat: pd.DataFrame, model_name: str = 'distiluse-base-multilingual-cased') -> pd.DataFrame:
    chat_ = chat.copy()
    embedder = SentenceTransformer(model_name)
    chat_['emb'] = chat_.apply(lambda row: embedder.encode(row['text']), axis=1)
    return chat_


def generate_msg_pairs(chat: pd.DataFrame, n_neg_samples: int = 25, window_width: int = 100, random_state: int = 451) -> pd.DataFrame:
    """
    Produces the whole dataset with positive and negative message pairs
    """
    chat = add_embeddings(chat)
    pairs_pos = pairs_pos = pd.merge(
        chat.drop(columns=c.REPLY_ID),
        chat,
        left_on=c.ID,
        right_on=c.REPLY_ID,
        suffixes=c.SUFFIXES,
        how='inner'
    ).drop(columns=c.REPLY_ID)
    chat = chat.sort_values(c.ID)
    pairs_neg = sample_negative_pairs(chat, n_neg_samples, window_width, random_state)
    pairs_pos[c.TARGET] = 1
    pairs_neg[c.TARGET] = 0

    dataset = pd.concat([pairs_pos, pairs_neg])
    return dataset


def generate_test_period_pairs(dataset_test: pd.DataFrame, max_pairs: int = 100) -> pd.DataFrame:
    """
    Generates all possible pairs for the test period, which is one week.
    :param max_pairs: Max amount of paired messages that follow the original message
    :returns: DataFrame with all the pairs for prediction, same suffix as other pair generators
    """
    cj_key = 'cross_join_key'
    dataset_test_ = dataset_test.copy()
    dataset_test_ = add_embeddings(dataset_test_)
    # doing this improvised cross join because week history is rather small in size
    # so this should not cause any out-of-memory problems
    dataset_test_[cj_key] = 0
    dataset_pairs = pd.merge(
        dataset_test_.drop(columns=c.REPLY_ID),
        dataset_test_,
        left_on=cj_key,
        right_on=cj_key,
        suffixes=c.SUFFIXES
    )
    dataset_pairs[c.TARGET] = dataset_pairs.eval(f'id_orig == {c.REPLY_ID}').astype(int)
    dataset_pairs = (
        dataset_pairs
        .query('(id_repl - id_orig > 0) & (id_repl - id_orig <= @max_pairs)')
        .drop(columns=[c.REPLY_ID, cj_key])
    )
    return dataset_pairs