import pandas as pd
import numpy as np
import src.constants as c
from typing import Tuple


def train_model(features: pd.DataFrame, model):
    y = features[c.TARGET]
    x = features.drop(columns=c.TARGET)
    model.fit(x, y)
    return None


def infer_model(features: pd.DataFrame, model):
    # saving indices to create better distance matrix w/
    features_ = features.drop(columns=c.TARGET)
    predictions = pd.DataFrame({
        'id_orig': features_.index.get_level_values('id_orig'),
        'id_repl': features_.index.get_level_values('id_repl'),
        'y': features[c.TARGET],
        'y_hat': model.predict(features_),
        'prob': model.predict_proba(features_)[:, 1]  # probability of y == 1
    })
    return predictions


def create_distance_matrix(predictions: pd.DataFrame) -> Tuple[np.array, dict]:
    # sort in case concat-ed ids are not monotonous
    ids = sorted(list(set(predictions['id_orig']).union(predictions['id_repl'])))
    id2idx = {idx: row_id for row_id, idx in enumerate(ids)}
    dist_matrix = np.zeros((len(ids), len(ids)), dtype=float)
    for row in predictions.to_dict(orient='records'):
        i, j, y_hat, y = id2idx[row['id_repl']], id2idx[row['id_orig']], row['prob'], row[c.TARGET]
        # since we want the output for clustering, not validation, we can use the target
        # if we set dist_matrix[i, j] = y = 1 for the replies then dist = 0, which could be awkward for clustering
        dist_matrix[i, j] = (y_hat + y) / 2.0
    return dist_matrix, {idx: row_id for row_id, idx in id2idx.items()}


def name_discussion(chat_clustered: pd.DataFrame) -> str:
    # page rank, graph stats, summarisation on complete topic?
    pass