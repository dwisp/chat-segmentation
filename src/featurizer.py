import pandas as pd
import numpy as np
import src.constants as c


def featurize_msg_pairs(pairs: pd.DataFrame, chat: pd.DataFrame) -> pd.DataFrame:
    """
    Simple featurizer that generates the features for supplied chat pairs
    Returns a DataFrame with features for a message pair + target and indexed with [id_orig, id_repl]
    """
    features = pairs.copy()
    chat_ = chat.copy()

    # author related features
    # how often does an author post
    author_idx = ['from_id_orig', 'from_id_repl']
    pairs_idx = ['id_orig', 'id_repl']
    # time diffs
    features['f_time_diff'] = (features['date_repl'] - features['date_orig']).dt.total_seconds() / 3600
    features['f_id_diff'] = features['id_repl'] - features['id_orig']
    features['f_time_per_msg'] = (features['f_time_diff'] / features['f_id_diff']).astype(float)
    # lengths
    features['f_len_orig'] = features['text_orig'].apply(len)
    features['f_len_repl'] = features['text_repl'].apply(len)

    # regex based
    re_url = 'http|www|\.ru'
    re_mention = '@'
    re_question = '\?'

    features['f_has_link_orig'] = features['text_orig'].str.contains(re_url).astype('int')
    features['f_has_link_repl'] = features['text_repl'].str.contains(re_url).astype('int')
    features['f_has_mention_orig'] = features['text_orig'].str.contains(re_mention).astype('int')
    features['f_has_mention_repl'] = features['text_repl'].str.contains(re_mention).astype('int')
    features['f_has_question_orig'] = features['text_orig'].str.contains(re_question).astype('int')
    features['f_has_question_repl'] = features['text_repl'].str.contains(re_question).astype('int')

    # misc
    features['f_same_author'] = (features['from_id_orig'] == features['from_id_repl']).astype('Int64')
    features['f_edited_orig'] = features['edited_orig'].notnull().astype('int')
    features['f_edited_repl'] = features['edited_repl'].notnull().astype('int')

    # wc
    # 4096 is the max message len
    features['f_wc_orig'] = features['text_orig'].str.lower().str.split().apply(len) / 4096
    features['f_wc_repl'] = features['text_repl'].str.lower().str.split().apply(len) / 4096

    # bow
    def jaccard(a: set, b: set) -> float:
        return len(a.intersection(b)) / (len(a.union(b)) + 0.1)

    def to_bag(text: str, n=4):
        text = text.lower()
        return {text[i:i + n] for i in range(len(text) - n)}

    features['f_bow_jaccard'] = features.apply(
        lambda row: jaccard(to_bag(row['text_orig']), to_bag(row['text_repl'])),
        axis=1
    )

    def cosine_sim(x: np.array, y: np.array) -> float:
        enum = np.sum(x * y)
        denom = np.sqrt(np.sum(x ** 2)) * np.sqrt(np.sum(y ** 2))
        return enum / denom

    features['f_emb_cosine_sim'] = features.apply(
        lambda row: cosine_sim(row['emb_orig'], row['emb_repl']),
        axis=1
    )

    # take features, target and index cols
    features = features[[col for col in features if col.startswith('f_')] + pairs_idx + [c.TARGET]]
    # remove filtering feature prefixes
    features.columns = features.columns.str.lstrip('f_')

    return features.set_index(pairs_idx)
