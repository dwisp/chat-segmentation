import pandas as pd
import json
import gzip
from typing import Union

import src.constants as c


def concat_textlike(msg: list) -> str:
    text = ''
    for entity in msg:
        if isinstance(entity, dict):
            text += entity[c.TEXT]
        elif isinstance(entity, str):
            text += entity
    return text


def process_msg(msg: Union[str, list]) -> str:
    if isinstance(msg, str):
        return msg
    elif isinstance(msg, list):
        return concat_textlike(msg)


def read_chat_from_json(path: str) -> pd.DataFrame:
    with gzip.open(path, 'rt', encoding='utf-8') as f:
        chat_json = json.load(f)
    drop_cols = ['type', 'width', 'height', 'mime_type', 'performer', 'title', 'duration_seconds']
    # columns which contain media info
    # bool_cols = ['photo', 'file', 'thumbnail']
    # columns which contain factor levels
    # factor_cols = ['forwarded_from', 'via_bot', 'media_type']
    
    msg = chat_json['messages']
    msg_df = pd.DataFrame([m for m in msg if m.get(c.TEXT)])
    
    msg_df[c.ID] = msg_df[c.ID].astype(pd.Int64Dtype())
    msg_df[c.REPLY_ID] = msg_df[c.REPLY_ID].astype(pd.Int64Dtype())
    msg_df[c.DATE] = pd.to_datetime(msg_df[c.DATE])
    msg_df[c.TEXT] = msg_df[c.TEXT].apply(process_msg)

    # we are only interested in clustering messages
    # but we won't detect memes with a hundred reactions or something
    msg_df = msg_df.query('type == "message"')
    msg_df = msg_df.drop(columns=drop_cols)
    return msg_df
