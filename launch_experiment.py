import click
from inspect import signature
import src.dataset_builder as b
import src.featurizer as f
import src.models as m
import src.utils as u
import hashlib
from humanhash import humanize
from sklearn.ensemble import GradientBoostingClassifier
from os.path import join


@click.option('--chat_path', type=str, help='Path to chat JSON file')
@click.option('--window_width', default=100, type=int, help='Window width for sampling the negative pairs from')
@click.option('--inference_window_width', default=100, type=int, help='Window width to sampling negative pairs from')
@click.option('--neg_pairs', default=2, type=int, help='How many negative pairs to sample for a single reply pair')
@click.option('--seed', default=451, type=int, help='Random seed for the sampling and training a model, etc')


def main(chat_path: str, window_width: int, inference_window_width: int, neg_pairs: int, seed: int):
    # creating experiment name
    args = ', '.join(str(val) for val in list(signature(main).parameters.values()))
    experiment_name = humanize(hashlib.md5(bytes(''.join(args))), words=2)
    out_path = join('./experiments/', experiment_name)

    chat = u.read_chat_from_json(chat_path)

    train = b.generate_msg_pairs(chat, neg_pairs, window_width, seed)
    infer = b.generate_msg_pairs(chat, inference_window_width, inference_window_width)
    features_train = f.featurize_msg_pairs(train)
    features_infer = f.featurize_msg_pairs(infer)

    model = GradientBoostingClassifier()
    m.train_model(features_train, model)
    output = m.infer_model(features_infer)

    # diss_matrix = m.create_distance_matrix(output)
    features_train.to_csv(join(out_path, 'features_train.csv'), index=False)
    features_infer.to_csv(join(out_path, 'features_infer.csv'), index=False)
